import React, { createContext, useState } from "react";

export const CharStats = createContext({
  name: "",
  str: 10,
  hp: 10,
  speed: 2,
  addStr: (item) => {},
  setName: (item) => {},
  setHp: (item) => {},
  setSpeed: (item) => {},
});

export const UserStatsProvider = ({ children }) => {
  const [name, setName] = useState("Arthus");
  const [str, addStr] = useState(20);
  const [hp, setHp] = useState(12);
  const [speed, setSpeed] = useState(2);

  return (
    <CharStats.Provider
      value={{
        name,
        str,
        hp,
        speed,
        addStr,
        setName,
        setHp,
        setSpeed,
      }}
    >
      {children}
    </CharStats.Provider>
  );
};

export const OpponentStatsProvider = ({ children }) => {
  const name = "Lish";
  const str = 10;
  const hp = 12;

  return (
    <CharStats.Provider
      value={{
        name,
        str,
        hp,
      }}
    >
      {children}
    </CharStats.Provider>
  );
};
