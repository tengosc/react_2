import React, { useContext } from "react";
import { CharStats } from "../../context";

const Opponent = (props) => {
  const { name, str, hp, speed } = useContext(CharStats);

  return (
    <div className="col-6">
      <h1>{props.poziom}</h1>
      <p>Name: {name}</p>
      <p>str: {str}</p>
      <p>HP: {hp}</p>
      <p>speed: {speed}</p>
    </div>
  );
};

export default Opponent;
