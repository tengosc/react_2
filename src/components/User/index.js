import React, { useContext, useState } from "react";
import { CharStats } from "../../context";
import "./style.css";

const User = (props) => {
  const { name, str, hp, speed, addStr, setName, setHp, setSpeed } = useContext(
    CharStats
  );
  const [showStats, setShowStats] = useState(false);
  const [myName, setMyName] = useState("");
  const randomStr = Math.floor(Math.random() * 10) + 1;
  const randomHp = Math.floor(Math.random() * 50) + 50;
  const randomSpeed = Math.floor(Math.random() * 5) + 1;

  const handleChange = (e) => {
    setMyName(e.target.value);
  };

  const handleSubmit = (e) => {
    e.preventDefault();

    setName(myName);
    addStr(randomStr);
    setHp(randomHp);
    setSpeed(randomSpeed);

    setShowStats(true);
  };

  return (
    <div className="col-6">
      {showStats ? (
        <div>
          <h1>{props.poziom}</h1>
          <p>Name: {name}</p>
          <p>str: {str}</p>
          <p>HP: {hp}</p>
          <p>speed: {speed}</p>

          <div onClick={() => addStr(str + 1)}>add +1 str</div>
        </div>
      ) : (
        <form onSubmit={handleSubmit}>
          <h2>Towrzenie postaci</h2>
          <br />
          <label>Imię: </label>
          <input
            type="text"
            name="name"
            value={myName}
            onChange={handleChange}
          />
          <br />
          <label>str: </label>
          <input type="text" name="str" value={randomStr} />
          <br />
          <label>hp: </label>
          <input type="text" name="hp" value={randomHp} />
          <br />
          <label>speed: </label>
          <input type="text" name="speed" value={randomSpeed} />
          <br />
          <input type="submit" name="send" />
        </form>
      )}
    </div>
  );
};

export default User;
