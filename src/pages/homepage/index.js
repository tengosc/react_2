import React from "react";
import User from "../../components/User";
import Opponent from "../../components/Opponent";
import { UserStatsProvider, OpponentStatsProvider } from "../../context";

const homepage = ({ title }) => {
  return (
    <div>
      <UserStatsProvider>
        <User poziom="10"></User>
      </UserStatsProvider>

      <OpponentStatsProvider>
        <Opponent poziom="10"></Opponent>
      </OpponentStatsProvider>
    </div>
  );
};

export default homepage;
